pipeline {
    agent any

    options {
        timestamps ()
        timeout (time: 10, unit: "MINUTES")
        gitLabConnection ("gitlab")
    }

    tools {
        maven "maven"
        jdk "openjdk-8"
    }

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }

    environment {
        REPO_URL = "git@gitlab.com:Terre8055/suggest-lib.git"
    }

    stages {

        stage ("Checkout") {
            steps {
                checkout scm
            }
        }

        stage ("Set Version") {
            when {
                branch "release/*"
            }
            steps {
                script {
                    sshagent(['image_pipe']){
                        sh"""
                            git fetch $REPO_URL --tags
                        """
                    }
                    env.VERSION=sh( returnStdout: true, script: "bash getTags.sh").trim()
                    println(env.VERSION)

                    sh"""
                        mvn versions:set -DnewVersion=$env.VERSION
                    """
                }
            }
        }

        stage ("Build, UnitTest & Package") {
            steps {
                script {
                    sh """
                        mvn verify
                    """
                }
            }
        }

        stage ("Publish") {
            when {
                anyOf {
                    branch "main"
                    branch "release/*"
                }
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn -Dmaven.test.skip=false deploy
                    """
                    }
                }
            }
        }

        stage ("Tag") {
            when {
                branch "release/*"
            }
            steps{
                script{
                    sshagent(['image_pipe']){
                        sh"""
                            git clean -f -x
                            git tag $env.VERSION || echo "tag already exists"
                            git push --tags
                        """
                    }

                }
            }
        }
    }

    post {

        always {
            script {
                cleanWs()
            }
        }

        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }

        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
    }
}