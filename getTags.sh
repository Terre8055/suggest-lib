#!/bin/bash

# Calculate version for release branches
function calculate_release_version() {
    local has_tags=$(git tag -l | wc -l)

    if [ "$has_tags" -eq 0 ]; then
        # If no tags, set version to 0.0.0
        echo "1.0.0"
    else
        local latest_tag=$(git describe --tags --abbrev=0)
        local patch=$(echo "$latest_tag" | awk -F '.' '{print $3}')
        ((patch++))
        echo "$latest_tag" | sed "s/\.[0-9]*$/.$patch/"
    fi
    echo "Tags Generated"
}


# Main execution
calculate_release_version
